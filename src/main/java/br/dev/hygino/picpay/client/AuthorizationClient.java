package br.dev.hygino.picpay.client;

import br.dev.hygino.picpay.client.dto.AuthorizationResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(
        url = "${client.authorization-service.url}", name = "AuthorizationClient"
)
public interface AuthorizationClient {
    @GetMapping
    public ResponseEntity<AuthorizationResponse> isAuthorized();
}
