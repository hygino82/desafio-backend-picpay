package br.dev.hygino.picpay.client.dto;

public record AuthorizationResponse(boolean authorized) {
}
