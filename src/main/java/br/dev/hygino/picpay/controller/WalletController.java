package br.dev.hygino.picpay.controller;

import br.dev.hygino.picpay.dto.CreateWalletDto;
import br.dev.hygino.picpay.entity.Wallet;
import br.dev.hygino.picpay.service.WalletService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/wallets")
@CrossOrigin("*")
public class WalletController {
    private final WalletService walletService;

    public WalletController(WalletService walletService) {
        this.walletService = walletService;
    }


    @PostMapping
    public ResponseEntity<Wallet> createWallet(@RequestBody @Valid CreateWalletDto dto) {
        Wallet wallet = walletService.createWallet(dto);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(wallet.getId()).toUri();

        return ResponseEntity.created(uri).body(wallet);
    }
}
