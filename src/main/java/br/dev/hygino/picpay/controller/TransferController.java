package br.dev.hygino.picpay.controller;

import br.dev.hygino.picpay.dto.CreateWalletDto;
import br.dev.hygino.picpay.dto.TransferDto;
import br.dev.hygino.picpay.entity.Transfer;
import br.dev.hygino.picpay.entity.Wallet;
import br.dev.hygino.picpay.service.TransferService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/transfer")
public class TransferController {
    private final TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @PostMapping
    public ResponseEntity<Transfer> transfer(@RequestBody @Valid TransferDto dto) {
        var resp = transferService.transfer(dto);
        return ResponseEntity.ok(resp);
    }
}
